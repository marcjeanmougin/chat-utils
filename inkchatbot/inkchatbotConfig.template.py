#!/usr/bin/python3
# -*- coding: utf-8 -*-

server = "chat.freenode.net" # Server
ircport = 6667 # Port
serverport = 9753 # Port
channel1 = "#inkscape-devel" # Channel
channel2= "#inkscape" # Channel
adminName = "jabiertxof" #Admin user, currently can exit bot
botnick = "inkchatbot" # Your bots nick. also is the user in rocketchat
exitCode = "bye " + botnick #Message to exit bot
rocketBotCode = "rocket.cat" #rocketChat bot code
rocketBotName = "inkchatbot-0.5" #rocketChat bot name
integrationToken = "XXXXXXXXXXXXXXXX" #rocketChat integration token
password = "XXXXXXXXXXXXXX"; #Password of the nick. also is the password in rocketchat
rocketchatChannelId = {channel1:"S8QyyXE5DNPi83KuL", channel2:"DdSRYnE833uGFna8J"} #RC channelIdToPost https://rocket.chat/docs/developer-guides/rest-api/channels/list/
rocketchatUserId = "XXXXXXXXXXXXXX" #autorized user id, see: https://rocket.chat/docs/developer-guides/rest-api/authentication/login/
rocketchatUserToken = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" #autorized user token
irc2rocketHook = {channel1: "https://chat.inkscape.org/hooksXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",channel2: "https://chat.inkscape.org/hooks/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"}


