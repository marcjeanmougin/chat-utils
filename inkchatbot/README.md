# Services

## InkChatBot

### Install daemon
CP your inkchatbot/inkchatbotConfig.template.py -> inkchatbot/inkchatbotConfig.py:
Edit XXXn values
Place inkchatbot anywere in the server not public [/home/chat-utils/inkchatbot/], here are the bot
You can run with this order and do a cron to run at reestart.

```
nohup /usr/bin/python3 /home/chat-utils/inkchatbot/inkchatbotDaemon.py  > /home/chat-utils/inkchatbot/log.txt 2>&1 &
```

### Configuration
```
server = "chat.freenode.net" # Server
ircport = 6667 # Port
serverport = 9753 # Port
channel1 = "#inkscape-devel" # Channel
channel2= "#inkscape" # Channel
adminName = "jabiertxof" #Admin user, currently can exit bot
botnick = "inkchatbot" # Your bots nick. also is the user in rocketchat
exitCode = "bye " + botnick #Message to exit bot
rocketBotCode = "rocket.cat" #rocketChat bot code
rocketBotName = "inkchatbot-0.5" #rocketChat bot name
integrationToken = "XXXXXXXXXXXXXXXX" #rocketChat integration token
password = "XXXXXXXXXXXXXX"; #Password of the nick. also is the password in rocketchat
rocketchatChannelId = {channel1:"S8QyyXE5DNPi83KuL", channel2:"DdSRYnE833uGFna8J"} #RC channelIdToPost https://rocket.chat/docs/developer-guides/rest-api/channels/list/
rocketchatUserId = "XXXXXXXXXXXXXX" #autorized user id, see: https://rocket.chat/docs/developer-guides/rest-api/authentication/login/
rocketchatUserToken = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" #autorized user token
irc2rocketHook = {channel1: "https://chat.inkscape.org/hooksXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",channel2: "https://chat.inkscape.org/hooks/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"}

```

### RocketChatIntegrations
Backup of scripts placed inside integrations rocketChat zone

#### Based on code from https://github.com/Orderchaos/ircbot
