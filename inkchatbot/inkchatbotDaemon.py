#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, sys, socket, multiprocessing, urllib , shlex, json, cgi, requests, re
from io import BytesIO
from http.server import HTTPServer, BaseHTTPRequestHandler

RocketIRCBotDIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(RocketIRCBotDIR))
from inkchatbotConfig  import * 

#This first variable is the socket we’ll be using to connect and communicate with the IRC server. Sockets are complicated and can be used for many tasks in many ways. 
#See here if you’d like more information on sockets: https://docs.python.org/3/howto/sockets.html. 
#For now just know that this will be used to establish a continuous connection with the IRC server while the bot is running to send and receive information.
#To connect to IRC we need to use our socket variable (ircsock) and connect to the server. IRC is typically on port port or 6697 (6697 is usually for IRC with SSL which is more secure). 
#We’ll be using port for our example. We need to have the server name (established in our Global Variables) and the port number inside parentheses so it gets passed as a single item to the connection.
ircsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ircsock.connect((server, ircport)) # Here we connect to the server using the port port
#lastuser = "" 
# Restrict to a particular path.
class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        
    def do_HEAD(self):
        self._set_headers()
    
    def do_POST(self):
        #global lastuser
        self._set_headers()
        form = cgi.FieldStorage(
            fp=self.rfile,
            headers=self.headers,
            environ={'REQUEST_METHOD': 'POST'}
        )
        
        
        messageid = shlex.quote(form.getvalue("message_id"))
        if messageid is not None and form.getvalue("token") == integrationToken:
            headers = {
                'X-Auth-Token': rocketchatUserToken ,
                'X-User-Id': rocketchatUserId,
            }
            params = (
                ('msgId', messageid),
            )
            response = requests.get('https://chat.inkscape.org/api/v1/chat.getMessage', headers=headers, params=params)
            finalmsg = response.json()
            #txt = ""
            #if lastuser != form.getvalue("user"):
            user = form.getvalue("user")
            txt = "<" + '\x03' + ("%02d" % (hash(user) % 14 + 2)) + user + '\x03' + "> ";
            #lastuser = form.getvalue("user")
            if finalmsg.get('message') is not None:
                if finalmsg["message"].get('editedAt') is not None:
                    txt = txt + "[Edited] ";
                if finalmsg["message"].get('attachments') is not None:
                    for attach in finalmsg["message"]["attachments"]:
                        if attach.get('description') is not None and \
                        attach.get('title_link') is not None and \
                        attach.get('title') is not None:
                            txt = txt + attach.get('description') + ", see on https://chat.inkscape.org/" .  attach.get('title_link') + " (" + attach.get('title') + ") \n"
            txt = txt + form.getvalue("text") + "\n";
            txt = re.sub(r'(^| )@(\w\w+)( |$|. )', r'\1@ \2 \3', txt, 20, re.MULTILINE)
            for line in txt.split("\n"):
                for msg in split_utf8(line , 510):
                    sendmsg(msg, form.getvalue("channel"))

def split_utf8(s , n):
    assert n >= 4
    start = 0
    lens = len(s)
    while start < lens:
        if lens - start <= n:
            yield s[start:]
            return # StopIteration
        end = start + n
        while '\x80' <= s[end] <= '\xBF':
            end -= 1
        assert end > start
        yield s[start:end]
        start = end

def connect():
    ircsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ircsock.connect((server, ircport)) # Here we connect to the server using the port port
    login()
    
def login():
    ircsock.send(bytes("USER "+ botnick +"  8 * :" + rocketBotName + "\n", "utf-8")) # user information
    ircsock.send(bytes("NICK "+ botnick +"\n", "utf-8")) # assign the nick to the bot
    joinchan(channel1)
    joinchan(channel2)

def joinchan(chan): # join channel(s).
    #The ‘bytes’ part and "utf-8” says to send the message to IRC as utf-8 encoded bytes. In Python 2 this isn’t necessary, but changes to string encoding in Python 3 makes this a requirement here. 
    #You will see this on all the lines where we send data to the IRC server.
    ircsock.send(bytes("JOIN "+ chan +"\n", "utf-8")) 
    #Something to note, the "\n” at the end of the line denotes to send a new line character. It lets the server know we’re finished with that command rather than chaining all the commands onto the same line.
    # After sending the join command, we want to start a loop to continually check for and receive new info from server until we get a message with ‘End of /NAMES list.’. 
    #This will indicate we have successfully joined the channel. The details of how each function works is described in the main function section below. 
    #This is necessary so we don't process the joining message as commands.
    ircmsg = ""
    try:
        while ircmsg.find("End of /NAMES list.") == -1: 
            ircmsg = ircsock.recv(2048).decode("utf-8")
            ircmsg = ircmsg.strip('\n\r')
            if ircmsg != "":
                print(bytes(ircmsg, "utf-8"))
                if ircmsg.find("You have not registered") != -1:
                    ircsock.send(bytes("PRIVMSG NickServ :identify "+ password +"\n", "utf-8"));
                if ircmsg.find("Connection timed out") != -1:
                    login()
                if ircmsg.find("PING :") != -1:
                    ping()
                if ircmsg.find("rocketirc has disconnected") != -1:
                    login()
        ircsock.send(bytes("PRIVMSG NickServ :identify "+ password +"\n", "utf-8"));
    except socket.error:
        connect()

#This function doesn’t need to take any arguments as the response will always be the same. Just respond with "PONG :pingis" to any PING. 
#Different servers have different requirements for responses to PING so you may need to adjust/update this depending on your server. I’ve used this particular example with Freenode and have never had any issues.
def ping(): # respond to server Pings.
    ircsock.send(bytes("PONG :pingis\n", "utf-8"))

#All we need for this function is to accept a variable with the message we’ll be sending and who we’re sending it to. We will assume we are sending to the channel by default if no target is defined. 
#Using target=channel in the parameters section says if the function is called without a target defined, example below in the Main Function section, then assume the target is the channel.
def sendmsg(msg, channel): # sends messages to the target.
    #With this we are sending a ‘PRIVMSG’ to the channel. The ":” lets the server separate the target and the message.
    ircsock.send(bytes("PRIVMSG "+ channel +" :"+ msg +"\r\n", "utf-8"))
    #Main function of the bot. This will call the other functions as necessary and process the information received from IRC and determine what to do with it.

def chunks(s, n):
    """Produce `n`-character chunks from `s`."""
    for start in range(0, len(s), n):
        yield s[start:start+n]

def toIRC():
    # Create server
    httpd = HTTPServer(("", serverport), SimpleHTTPRequestHandler)
    print("Serving on localhost:" + str(serverport) + "...")
    httpd.serve_forever()
    
def toRocketChat():
    users = {}
    index = 0
    gap = 0
    while 1:
        #Here we are receiving information from the IRC server. IRC will send out information encoded in utf-8 characters so we’re telling our socket connection to receive up to 2048 bytes and decode it as utf-8 characters. 
        #We then assign it to the ircmsg variable for processing.
        ircmsg = ircsock.recv(2048).decode("utf-8", "ignore")
        #This part will remove any line break characters from the string. If someone types in "\n” to the channel, it will still include it in the message just fine. 
        #This only strips out the special characters that can be included and cause problems with processing.
        ircmsg = ircmsg.strip('\n\r')
        #This will print the received information to your terminal. You can skip this if you don’t want to see it, but it helps with debugging and to make sure your bot is working.
        #Here we check if the information we received was a PRIVMSG. PRIVMSG is how standard messages in the channel (and direct messages to the bot) will come in. 
        #Most of the processing of messages will be in this section.
        if ircmsg.find("PRIVMSG") != -1:
            #Above we split out the name, here we split out the message.
            name = ircmsg.split('!',1)[0][1:]
            if users.get(name) is None:
                if len(users) == 13:
                    for user in users:
                        if users[user] == 0:
                            del users[user]
                            users[name] = 0
                            break
                    gap += 1
                    index = 0
                else:
                    users[name] = index
                    index += 1
            indexlogo = (users[name] + gap) % 13
            irclogo = "https://media.inkscape.org/dl/resources/file/irc" + str(indexlogo) + ".png"
            messages = ircmsg.split('\n:',999)
            counter = 0
            messagequoted = ""
            for message in messages:
                counter += 1
                #First we want to get the nick of the person who sent the message. Messages come in from from IRC in the format of ":[Nick]!~[hostname]@[IP Address] PRIVMSG [channel] :[message]”
                #We need to split and parse it to analyze each part individually.
                message   = message.split('PRIVMSG',1)[1]
                onchannel = message.split(':',1)[0].strip()
                message   = message.split(':',1)[1]
                #Now that we have the name information, we check if the name is less than 17 characters. Usernames (at least for Freenode) are limited to 16 characters. 
                #So with this check we make sure we’re not responding to an invalid user or some other message.
                if len(name) < 17:
                    #And this is our first detection block! We’ll use things like this to check the message and then perform actions based on what the message is. 
                    #With this one, we’re looking to see if someone says Hi to the bot anywhere in their message and replying. Since we don’t define a target, it will get sent to the channel.
                    if message.find('Hi ' + botnick) != -1:
                        sendmsg("Hello " + name + "!", chan)
                    elif name != botnick:
                        if counter > 1:
                            messagequoted += "\n"
                        messagequoted += message

                if messagequoted != "":
                    lastmessage = ""
                    headers = {
                        'X-Auth-Token': rocketchatUserToken ,
                        'X-User-Id': rocketchatUserId,
                        'Content-type' :'application/json',
                    }
                    params = ()
                    response = requests.get("https://chat.inkscape.org/api/v1/channels.list", headers=headers, params=params)
                    channels = response.json()
                    for currentchannel in channels["channels"]:
                        if currentchannel.get("_id") == rocketchatChannelId.get(onchannel):
                            if currentchannel.get("lastMessage").get("alias") == name:
                                lastmessage = currentchannel.get("lastMessage").get("_id")
                    if lastmessage != "":
                        params = ()
                        response = requests.get('https://chat.inkscape.org/api/v1/chat.getMessage?msgId=' + lastmessage, headers=headers, params=params)
                        messagecontent = response.json()
                        originalmessage = messagecontent["message"].get("msg")
                        roomid = messagecontent["message"].get("rid")
                        params = {"roomId":roomid, "msgId":lastmessage, "text":originalmessage + "\n" + messagequoted}
                        response = requests.post('https://chat.inkscape.org/api/v1/chat.update', headers=headers, json=params)
                        #messagecontent = response.json()
                    else:
                        params = { "name": name, "text": messagequoted, "logo": irclogo}
                        response = requests.post(irc2rocketHook.get(onchannel), headers=headers, json=params)
            
            if name.lower() == adminName.lower() and message.rstrip() == exitCode:
                sendmsg("oh...okay. :'(", chan)
                ircsock.send(bytes("QUIT \n", "utf-8"))
                return
            #If the message is not a PRIVMSG it still might need some response.
            #Check if the information we received was a PING request. If so, we call the ping() function we defined earlier so we respond with a PONG.
        if ircmsg.find("PING :") != -1:
            ping()

#Once we’ve established the connection we need to send some information to the server to let the server know who we are. We do this by sending our user information followed by setting the nickname we’d like to go by.
#Finally, now that the main function is defined, we need some code to get it started.
login()

t1 = multiprocessing.Process(target=toRocketChat)
t2 = multiprocessing.Process(target=toIRC)
t1.start()
t2.start()
